import json
import os
import socket
import threading
from server import Server
from directory import Directory
from loader import Loader

PATH = "/home/developer/develop/test_git/www"
DELAY = 5
IP = "127.0.0.1"
PORT = 6666

SERV_IP = "127.0.0.1"
SERV_PORT = 55555


class Client(Server):

    dir_list = []
    loader = Loader()

    def init_dirs(self):
        for val in os.listdir(PATH):
            self.dir_list.append(Directory(PATH + '/' + val, loader))


    def get_dir(self, val):
        for dir in self.dir_list:
            if dir.name == val:
                return dir
    

    def handle(self, message):
        in_data = json.loads(str(message, 'ascii'))
        header = in_data['header']
        cmd = in_data['cmd']

        if cmd == 'refresh':
            dir = self.get_dir(in_data['dir_name'])
            
            if header['num'] == 0:
                dir.clear_update_list()
                dir.current_group = header['group_id']

            if dir.current_group == header['group_id']:
                print('Receive ' + in_data['file'] + ' | ID: ' + str(header['group_id']) + ' Num: ' + str(header['num']) + ' Amount: ' + str(header['amount']))
                dir.add_update_file(in_data['file'], in_data['hash'])
                if header['num'] == header['amount'] - 1:
                    dir.hard_update()
                    pass # dir update --hard

        if cmd == 'file_list':
            pass

        if cmd == 'dir_list':
            pass


    def get_status(self):
        res = {}
        header = {}
        data = {}
        header["ip_addr"] = IP
        header["port"] = PORT
        res["header"] = header
        for dir in self.dir_list:
            dir.update()
            data[dir.name] = dir.hashList
        res["data"] = data
        return res


    def package_dir(self, name, cmd):
        res = {}
        header = {}
        header["ip_addr"] = IP
        header["port"] = PORT
        res["header"] = header
        res['dir_name'] = name
        res['cmd'] = cmd
        return res


    def refresh_all_dirs(self, ip, port):
        # self.send(ip, port, json.dumps(self.get_status()))
        for dir in self.dir_list:
            dir.update()
            self.send(ip, port, json.dumps(self.package_dir(dir.name, 'refresh')))

    def start_request(self):
        self.request_tick()
        # self.timer = threading.Timer(DELAY, self.request_tick)
        # self.timer.start()


    def request_tick(self):
        print("Tick")
        client.refresh_all_dirs(SERV_IP, SERV_PORT)
        self.timer = threading.Timer(DELAY, self.request_tick)
        self.timer.start()



if __name__ == "__main__":
    client = Client(IP, PORT)
    client.init_dirs()
    client.start_request()
    client.start_server()
    print("Server started. Address: " + IP + " Port: " + str(PORT))
    client.loop()
    client.stop_server()