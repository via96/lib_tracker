import hashlib
import sys
import os
import time
from loader import Loader

class Directory:

    def __init__(self, path, loader):
        self.dirpath = path
        if self.dirpath[-1] == '/': self.dirpath[:len(self.dirpath)-1:]
        self.name = self.dirpath.split('/')[-1]
        self.loader = loader


    dirpath = ''
    name = ''
    hashList = {}
    current_group = -1
    update_list = {}


    def parse_name(self, path):
        return path.replace(self.dirpath+'/', '', 1)
        # return path.split('/')[-1]


    def __eq__(self, other):
        return self.name == other.name and self.hashList == other.hashList


    def __ne__(self, other):
        return not self.__eq__(other)


    def get_hash(self, path):
        with open(path, 'rb') as f:
            m = hashlib.md5()
            while True:
                data = f.read(8192)
                if not data:
                    break
                m.update(data)
            return m.hexdigest()


    def find_files(self, catalog):
        file_list = []
        for root, dirs, files in os.walk(catalog):
            file_list += [os.path.join(root, name) for name in files]
        return file_list


    def load(self):
        list = {}
        for file in self.find_files(self.dirpath):
            list[self.parse_name(file)] = self.get_hash(file)
        return list


    def hard_update(self):
        # for name, hash in self.hashList.items():
        #     if not self.update_list.__contains__(name):
        #         loader.remove_file(name)
        #         pass # delete file

        for name, hash in self.update_list.items():
            if not self.hashList.__contains__(name):
                self.loader.load_file(name)
                pass # add file
            if self.hashList[name] != hash:
                self.loader.load_file(name)
                pass # update file

        print('Directory ' + self.name + ' hard update')
        pass


    def update(self):
        self.hashList = self.load()


    def isUpdated(self):
        return self.hashList != self.load()


    def update_dict(self):
        res = {}
        load = self.load()
        for val in self.hashList.keys():
            if not load.__contains__(val):
                res[val] = 'delete'
        for val in load:
            if not self.hashList.__contains__(val):
                res[val] = 'add'
                continue
            if load[val] != self.hashList[val]:
                res[val] = 'update'
        return res


    def equals(self, dir_list):
        return self.hashList == dir_list


    def clear_update_list(self):
        self.update_list = {}


    def add_update_file(self, file, hash):
        self.update_list[file] = hash

if __name__ == "__main__":
    dir = Directory("/home/developer/develop/Projects/libs", Loader())
    while True:
        time.sleep(1)
        if dir.isUpdated():
            dir.update()
            print("Update " + dir.name)
