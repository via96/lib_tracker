import socket
import time
import queue
import threading
from codex_queue import ServerQueue
from enum import Enum

DELAY = 1



class Message:
    ip = ''
    port = -1
    data = ''

    def __init__(self, ip, port, data):
        self.ip = ip
        self.port = port
        self.data = data
    


class Sender(threading.Thread):

    out_queue = queue.Queue()
    lock = threading.Lock()
    msg_id = 1

    def __init__(self):
        threading.Thread.__init__(self)


    def add_message(self, mes):
        with self.lock:
            self.out_queue.put(mes)


    def __send_from_queue__(self):
        with self.lock:
            if not self.out_queue.empty():
                mes = self.out_queue.get()
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                sock.connect((mes.ip, mes.port))
                try:
                    sock.sendall(bytes(mes.data, 'ascii'))
                finally:
                    sock.close()
                print('Send message ' + str(self.msg_id))
                self.msg_id += 1
                sock.close()


    def run(self):
        while True:
            self.__send_from_queue__()



class Server:

    out_queue = queue.Queue()

    def __init__(self, ip, port):
        self.queue = ServerQueue(ip, port)
        self.sender = Sender()
        self.sender.start()


    def start_server(self):
        self.queue.start_server()


    def stop_server(self):
        self.queue.stop_server()


    def loop(self):
        while True:
            self.process()
            if self.queue.exists():
                self.handle(self.queue.get())


    def send(self, ip, port, message):
        self.sender.add_message(Message(ip, port, message))


    def handle(self, message):
        """
        Prototype
        """
        pass
    
    
    def process(self):
        pass
