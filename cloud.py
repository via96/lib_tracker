import os
import json
from server import Server
from directory import Directory

PATH = "/home/developer/develop/test_git/qqq"

IP = "127.0.0.1"
PORT = 55555

class Cloud(Server):
    
    dir_list = []
    msg_group_sequence = 0


    def init_dirs(self):
        for val in os.listdir(PATH):
            dir = Directory(PATH + '/' + val)
            print("Init " + dir.name)
            self.dir_list.append(dir)


    def dir_contains(self, val):
        for dir in self.dir_list:
            if dir.name == val:
                return True
        return False


    def get_dir(self, val):
        for dir in self.dir_list:
            if dir.name == val:
                return dir


    def get_group_id(self):
        self.msg_group_sequence = (self.msg_group_sequence+1) % 100000
        return self.msg_group_sequence


    def get_dir_names(self):
        res = []
        for dir in self.dir_list:
            res.append(dir.name)
        return sorted(res)
    

    def process_state(self, message):
        in_data = json.loads(str(message, 'ascii'))
        header = in_data["header"]
        cmd = in_data["cmd"]
        group_id = self.get_group_id()

        if cmd == 'refresh' or cmd == 'file_list':
            i = 0
            dir_name = in_data["dir_name"]
            dir = self.get_dir(dir_name)
            dir.update()
            file_amount = len(dir.hashList)

            for file_path, hash in dir.hashList.items():
                out_data = {}

                out_header = {}
                out_header['ip_addr'] = IP
                out_header['port'] = PORT
                out_header['group_id'] = group_id
                out_header['num'] = i
                out_header['amount'] = file_amount

                out_data['header'] = out_header
                out_data['dir_name'] = dir_name
                out_data['file'] = file_path
                out_data['hash'] = hash
                out_data['cmd'] = cmd

                self.send(header['ip_addr'], header['port'], json.dumps(out_data))
                i += 1

        if cmd == 'dir_list':
            i = 0
            for dir in self.dir_list:
                out_data = {}

                out_header = {}
                out_header['ip_addr'] = IP
                out_header['port'] = PORT
                out_header['group_id'] = group_id
                out_header['num'] = i
                out_header['amount'] = len(self.dir_list)

                out_data['header'] = out_header
                out_data['dir_name'] = dir.name
                out_data['file'] = file_path
                out_data['cmd'] = cmd

                self.send(header['ip_addr'], header['port'], json.dumps(out_data))
                i += 1
            pass


    def handle(self, message):
        print(str(message, 'ascii'))
        self.process_state(message)
        # print(str(message, "ascii"))
        # str_mes = str(message, 'ascii'))
    

    def process(self):
        for dir in self.dir_list:
            if dir.isUpdated():
                print("Update " + dir.name)
                dir.update()
        


if __name__ == "__main__":
    server = Cloud(IP, PORT)
    server.init_dirs()
    server.start_server()
    server.loop()
    server.stop_server()