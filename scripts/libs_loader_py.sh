#!/usr/bin/expect

set PASS "qwedcxzas"

spawn scp -r administrator@192.168.88.92:/mnt/git_storage/Release/ ..
expect "administrator@192.168.88.92's password: " { send $PASS\r }
expect eof
