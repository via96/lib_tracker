import subprocess

LOADER_SH = 'libs_loader_py.sh'
PARSER_SH = 'libs_parser_py.sh'

class Loader:

    @staticmethod
    def load_sh(dir_path):
        if dir_path[-1] != '/': 
            dir_path = dir_path + '/'
        subprocess.call(dir_path + LOADER_SH)
        print('load')
        subprocess.call(dir_path + PARSER_SH)
        print('parse')



if __name__ == '__main__':
    Loader.load_sh("/home/developer/develop/Projects/libs/")