# import paramiko

# host = "192.168.1.5"
# port = 22
# transport = paramiko.Transport((host, port))
# transport.connect(username='dima', password='-JokeR-')
# sftp = paramiko.SFTPClient.from_transport(transport)

# remotepath = '/home/dima/temp/q'
# localpath = '/home/via/dev/test_1/q'

# sftp.get(remotepath, localpath)
# # sftp.put(localpath, remotepath)

# sftp.close()
# transport.close()

import paramiko, os
from threading import Lock
paramiko.util.log_to_file('/tmp/paramiko.log')
host = "192.168.1.5"
port = 22
password = "-JokeR-"
username = "dima"
remotepath = '/home/dima/temp'
localpath = '/home/via/dev/test_1/temp1'

class Loader:

    lock = Lock()

    def __init__(self):
        self.transport = paramiko.Transport((host, port))
        self.transport.connect(username = username, password = password)
        self.sftp = paramiko.SFTPClient.from_transport(self.transport)


    def load_file(self, file_path):
        with self.lock:
            self.sftp.get(remotepath +'/' + file_path, localpath + '/' + file_path)
            print('Load file ' + file_path.split('/')[-1])


    def remove_file(self, file_path):
        with self.lock:
            os.remove(localpath + '/' + file_path)
            print('Remove file ' + file_path.split('/')[-1])

    
    def remove_dir(self, name):
        with self.lock:
            os.rmdir(localpath + '/' + name)
            print('Remove directory ' + name)


# for path,files  in sftp_walk(remotepath):
#     for file in files:
#         #sftp.get(remote, local) line for dowloading.
#         path_tmp = str(localpath + path.replace(remotepath, '')).split('/')
#         del path_tmp[-1]
#         loc_path = 
#         sftp.get(os.path.join(os.path.join(path,file)), localpath + path.replace(remotepath, ''))